import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/enviroments/enviroment';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) { }

  getWeatherForecast(location: string): Observable<any> {
    const url = `${environment.WEATHER_API}${location}/31,80/forecast`;
    return this.http.get(url);
  }

}
