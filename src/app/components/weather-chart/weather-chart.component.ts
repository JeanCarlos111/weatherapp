import { Component } from '@angular/core';
import { WeatherService } from 'src/app/services/weather.service';
import Chart from 'chart.js/auto';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-weather-chart',
  templateUrl: './weather-chart.component.html',
  styleUrls: ['./weather-chart.component.scss']
})
export class WeatherChartComponent {
  constructor(
    private route: ActivatedRoute,
    private weatherService: WeatherService
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      const location = params.get('location');
      if (location)
        this.weatherService.getWeatherForecast(location).subscribe(data => {
          const periods = data.properties.periods;
          const temperatures = periods.map((period: any) => period.temperature);
          const labels = periods.map((period: any) => period.name);
          const temperatureUnit = periods[0].temperatureUnit; 
          this.renderChart(labels, temperatures, temperatureUnit);
        });
    });
  }

  renderChart(labels: string[], temperatures: number[], temperatureUnit: string): void {
    const canvas = <HTMLCanvasElement>document.getElementById('weatherChart');
    const ctx = canvas.getContext('2d') as any;

    new Chart(ctx, {
      type: 'line',
      data: {
        labels: labels,
        datasets: [{
          label: `Temperature °(${temperatureUnit})`,
          data: temperatures,
          borderColor: 'rgba(255, 99, 132, 1)',
          borderWidth: 2,
          fill: false
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: false
          }
        }
      }
    });
  }
}
