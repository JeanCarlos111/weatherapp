import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WeatherChartComponent } from './components/weather-chart/weather-chart.component';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'weather/:location', component: WeatherChartComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
